#!/bin/bash -e
  cd files
  cp stacks-node.service "${ROOTFS_DIR}/etc/systemd/system/"
  mkdir -p "${ROOTFS_DIR}/etc/stacks-blockchain/"
  cp xenon-miner-conf.toml "${ROOTFS_DIR}/etc/stacks-blockchain/"
  cp bashrc "${ROOTFS_DIR}/home/stacks/"
  #mv "${ROOTFS_DIR}/home/stacks/bashrc" "${ROOTFS_DIR}/home/stacks/.bashrc"
  mkdir -p bin
  cd bin
  wget https://stackspack.s3-us-west-2.amazonaws.com/bins/blockstack-cli
  wget https://stackspack.s3-us-west-2.amazonaws.com/bins/blockstack-core
  wget https://stackspack.s3-us-west-2.amazonaws.com/bins/clarity-cli
  wget https://stackspack.s3-us-west-2.amazonaws.com/bins/stacks-node
  install -m 0755 -o root -g root -t "${ROOTFS_DIR}/usr/local/bin/" *
