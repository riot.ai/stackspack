#!/bin/bash -e
# https://github.com/nodesource/distributions/blob/master/README.md#debinstall
# Using Debian, as root
wget https://bitcoin.org/bin/bitcoin-core-0.20.1/bitcoin-0.20.1-arm-linux-gnueabihf.tar.gz
tar zxvf bitcoin-0.20.1-arm-linux-gnueabihf.tar.gz
install -m 0755 -o nobody -g nogroup -t /usr/local/bin bitcoin-0.20.1/bin/*

#!/bin/bash -e
  cd files
  cp bitcoind.service "${ROOTFS_DIR}/etc/systemd/system/"
  mkdir -p "${ROOTFS_DIR}/etc/bitcoin/"
  cp bitcoin.conf "${ROOTFS_DIR}/etc/bitcoin/"
